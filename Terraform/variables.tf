provider "aws"{
          region= var.aws_region
          profile = "dbs"
}

variable "region" {
          description = "Region Name"
          default = "eu-west-1"
}

variable "aws_region"{
          description = "EC2 Region for the VPC"
          default = "eu-west-1"
}

variable "remote_cidr"{
          description = "CIDR from the remote testing source"
          default = "0.0.0.0/0"
}

variable "vpc_cidr"{
          description = "CIDR for the whole VPC"
          default = "10.0.0.0/16"
}

variable "public_subnet1_cidr"{
          description = "CIDR for the public subnet 1"
          default = "10.0.1.0/24"
}

variable "private_subnet1_cidr"{
          description = "CIDR for the private subnet 1"
          default = "10.0.2.0/24"
}


variable "key_path"{
          description = "SSH public key path"
          default = "/root/.ssh/id_rsa.pub"
}

variable "private_key_path"{
          description = "SSH private key path"
          default = "/root/.ssh/id_rsa"
}

variable "asg_ec2_instance_min"{
          description = "Autoscaling Jenkins Minimum Scale"
          default = "1"
}

variable "asg_ec2_instance_max"{
          description = "Autoscaling Jenkins Maximum Scale"
          default = "2"
}

variable "asg_ec2_instance_desired"{
          description = "Autoscaling jenkins slave desired scale"
          default = "2"
}


variable "data_volume_type"{
          description = "EBS Volume type"
          default = "gp2"
}

variable "data_volume_size"{
          description = "EBS volume size"
          default = "50"
}

variable "root_block_device_size"{
          description = "EBS root volume size"
          default = "50"
}

variable "availability_zones"{
          default = "eu-west-1a"
}
