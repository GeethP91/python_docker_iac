output "My-App_Public_IP"{
    value = "${aws_instance.My-App.public_ip}"
}

output "My-App_Instance_ID"{
    value = "${aws_instance.My-App.id}"
}

output "My-App_AMI_ID"{
    value = "${data.aws_ami.ubuntu.id}"
}

output "Application-Repo"{
       value = "${aws_ecr_repository.application.repository_url}"
}

