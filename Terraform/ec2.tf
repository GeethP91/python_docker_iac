locals {
  ec2_user = "ubuntu"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_key_pair" "default" {
  key_name   = "ec2_key_pair"
  public_key = file("${var.key_path}")
}

resource "aws_security_group" "my-sg"{
          name = "my-sg"
          vpc_id = aws_vpc.default.id
          description = "Allowing incoming NFS ports"

          ingress{
            from_port = 5000
            to_port = 5000
            protocol = "tcp"
            cidr_blocks = [var.public_subnet1_cidr, var.private_subnet1_cidr,var.remote_cidr]
           }

          ingress{
            from_port = 22
            to_port = 22
            protocol = "tcp"
            cidr_blocks = [var.public_subnet1_cidr, var.private_subnet1_cidr,var.remote_cidr]
           }

          ingress{
            from_port = 80
            to_port = 80
            protocol = "tcp"
            cidr_blocks = [var.public_subnet1_cidr, var.private_subnet1_cidr]
           }

          egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
          }

         tags = {
           Name = "my-sg"
}
}

resource "aws_instance" "My-App" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.default.id
  security_groups = [aws_security_group.my-sg.id]
  subnet_id = aws_subnet.public_subnet1.id
  associate_public_ip_address = true
  provisioner "remote-exec" {
    inline = [
     "sudo apt-get update -y && apt-get -y upgrade",
     "sudo python3 -V",
     "sudo apt-get install -y python3-pip && apt-get update -y",
     "sudo pip3 --version && pip install --upgrade pip",
     "sudo pip3 install Flask",
     "sudo python3 -m flask --version",
     "echo Installed Python,Pip,Flask successfully",
     "sudo apt-get update -y",
     "sudo apt-get install docker.io -y",   
    ]

  connection {
      type = "ssh"
      user = "ubuntu"
      host = aws_instance.My-App.public_ip
      private_key = file("${var.private_key_path}")
   }
}
  tags = {
           Name = "My-App"
  }

}

