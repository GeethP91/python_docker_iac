FROM nginx:alpine

COPY /src/templates/index.html /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/*

COPY nginx.conf /etc/nginx/conf.d


